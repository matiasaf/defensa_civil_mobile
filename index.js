// import { Navigation } from 'react-native-navigation';
//
// import { registerScreens } from './src/screens';
//
// registerScreens(); // this is where you register all of your app's screens
//
// // start the app
// Navigation.startTabBasedApp({
//   tabs: [
//     {
//       label: 'Login',
//       screen: 'defensa.LoginForm', // this is a registered name for a screen
//       icon: require('./src/imgs/icon1.png'),
//       selectedIcon: require('./src/imgs/icon1_selected.png'), // iOS only
//       title: 'Logearse'
//     },
//     {
//       label: 'Register',
//       screen: 'defensa.RegisterUserPage',
//       icon: require('./src/imgs/icon1.png'),
//       selectedIcon: require('./src/imgs/icon1_selected.png'), // iOS only
//       title: 'Registrar'
//     }
//   ]
// });


import React from 'react';
import {AppRegistry,View} from 'react-native';
import Header from './src/components/Header';
import LoginForm from './src/components/LoginForm';
import AlertsAdd from './src/components/AlertsAdd';
import NewUserSection from './src/components/NewUserSection';
import RegisterUserPage from './src/RegisterUserPage/RegisterUserPage';
import AdminAlertsPending from './src/AdminAlertsPending/AdminAlertsPending';
//import App from './App';

const App = () => (
  <View>
    <Header />

    {/* <LoginForm /> */}
    {/* <AlertsAdd /> */}
    <RegisterUserPage />
    {/* <AdminAlertsPending /> */}

  </View>
  );

AppRegistry.registerComponent('HelloWorld', () => App);

import React, {Component} from 'react';
import {View, Modal, TouchableHighlight, Text, Alert} from 'react-native';
import AlertsList from '../components/AlertsList';

class AdminAlertsPending extends Component {

  state = {
    alertsPending: [
      {
        id: 1,
        name: 'tormentas fuertes.'
      },
      {
        id: 2,
        name: 'Vientos huracanados.'
      },
      {
        id: 3,
        name: 'Fuego en todos lados.'
      },
    ],
    modalVisible: false
  }

  setModalVisible = (modalVisible) => {
    this.setState({modalVisible});
  }

  onHandleAcceptAlert = (alertId) => {

  Alert.alert(
      // This is Alert Dialog Title
      'Alert Dialog Title',

      // This is Alert Dialog Message.
      'Alert Dialog Message',
      {
        text: 'Aceptar',
        onPress: () => console.log(`La alerta de id ${alertId} ha sido aceptada.`)
      },
      {
        text: 'Cancelar',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }
    )

  }

  render() {

    const {alertsPending} = this.state;

    return (<View>
      {alertsPending && <AlertsList alertsPending={alertsPending} onHandleAcceptAlert={this.onHandleAcceptAlert}/>}
    </View>)
  }

}

export default AdminAlertsPending;

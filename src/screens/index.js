import { Navigation } from 'react-native-navigation';

import LoginForm from '../components/LoginForm';
import RegisterUserPage from '../RegisterUserPage/RegisterUserPage';
import AdminAlertsPending from '../AdminAlertsPending/AdminAlertsPending';

// register all screens of the app (including internal ones)
export function registerScreens() {
  Navigation.registerComponent('defensa.LoginForm', () => LoginForm);
  Navigation.registerComponent('defensa.RegisterUserPage', () => RegisterUserPage);
  Navigation.registerComponent('defensa.AdminAlertsPending', () => AdminAlertsPending);
}

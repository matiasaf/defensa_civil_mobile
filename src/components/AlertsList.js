import React from 'react';
import {View, TouchableHighlight, Text, ScrollView} from 'react-native';
import AlertItem from './AlertItem';

const AlertsList = ({alertsPending, onHandleAcceptAlert}) => (
  <ScrollView>
    {alertsPending.map((alert, index) => <AlertItem key={index} alert={alert} onHandleAcceptAlert={onHandleAcceptAlert}  />)}
  </ScrollView>
)

export default AlertsList;

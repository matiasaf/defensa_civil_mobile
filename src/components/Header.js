import React from 'react';
import {Image, Text, View} from 'react-native';


const Header = ({title}) => {

  const {textStyle, viewStyle, imageStyle} = styles;

  return (<View style={viewStyle}>
    <Image source={require('../imgs/defensa_header.png')} style={imageStyle} />
    {/* <Text style={textStyle}>{title}</Text> */}

  </View>);
}

const styles = {
  viewStyle: {
    justifyContent:'center',
    alignItems:'center',
    height:75,

  },
  imageStyle: {

  },
  textStyle: {
    fontSize: 30,
  }
}

export default Header;

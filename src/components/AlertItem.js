import React from 'react';
import {TouchableHighlight, Text} from 'react-native';

const AlertItem = ({alert, onHandleAcceptAlert}) => (
  <TouchableHighlight onPress={() => onHandleAcceptAlert(alert.id)}>
    <Text style={styles.textStyle}>{alert.name}</Text>
  </TouchableHighlight>
);

const styles = {
  textStyle :{
    fontSize: 15,
    padding:10
  }

};


export default AlertItem;

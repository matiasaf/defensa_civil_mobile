import React, {Component} from 'react';
import {View, Text, TextInput, Button} from 'react-native';
import axios from 'axios';
import listElements from '../json_elements/list';

class AlertsAdd extends Component {

  state = {
    tipoAlerta: '',
    descripcionAlerta: '',
    regionAlerta: '',
    albums: []
  }

  componentWillMount() {

    // let that = this;

    // setTimeout( () => {
    //   that.setState({albums: listElements});
    // } , 500);

    axios.get('https://rallycoding.herokuapp.com/api/music_albums')
      .then(response => this.setState({albums: response.data}))

  }

  render() {


    const {albums} = this.state;

    return (<View>

      {albums && albums.map((album, index) => <Text key={index}>{album.title}</Text>)}

    </View>)

  }

}

export default AlertsAdd;

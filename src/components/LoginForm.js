import React, {Component} from 'react';
import {View, Text, TextInput, Button} from 'react-native';

class LoginForm extends Component {

  state = {
    username: '',
    password:''
  }

  componentWillMount(){
    console.log('se monto el componente');
  }

  onPressAcceder = () => {
    console.log(this.state.username + "  "+this.state.password);
    console.log(this);
  }

  onHandleAddNewUser = () =>{

    console.log('agregando nuevo usuario');

  }


  render() {

    const {textTitleStyle, inputUsuario, buttonStyle} = styles;

    return (<View>

      <Text style={textTitleStyle}>Defensa Civil</Text>

      <TextInput
        style={inputUsuario}
        placeholder="Usuario :"
        onChangeText={(username) => this.setState({username})}
      />

      <TextInput
        style={inputUsuario}
        placeholder="Contraseña :"
        onChangeText={(password) => this.setState({password})}
      />

      <Button
        onPress={() => this.onPressAcceder()}
        title="Acceder"
        color="#841584"
        accessibilityLabel="Presione para acceder"
      />

        <Text style={styles.textNewUser}>¿Nuevo usuario?</Text>
        <Button
          style={styles.button}
          onPress={() => this.onHandleAddNewUser()}
          title="Crear Usuario"
        />

    </View>)
  }
}

const styles = {
  textTitleStyle: {
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'serif',
    marginTop: 30,
  },
  textNewUser : {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 30
  },
  inputUsuario: {
    height: 50
  },
  button : {
    marginTop: 30
  }
}

export default LoginForm;

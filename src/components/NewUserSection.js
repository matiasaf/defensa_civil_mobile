import React from 'react';
import {View, StyleSheet, Text, Button} from 'react-native';

const NewUserSection = ({onHandleAddNewUser}) => (
  <View>
    <Text style={styles.textNewUser}>¿Nuevo usuario?</Text>
    <Button
      style={styles.button}
      onPress={() => onHandleAddNewUser()}
      title="Crear Usuario"
    />
  </View>
)

const styles = StyleSheet.create({
  textNewUser : {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 30
  },
  button : {
    marginTop: 30
  }

})


export default NewUserSection;

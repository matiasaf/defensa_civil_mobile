import React, {Component} from 'react';
import { View , Text, TextInput, Button, Alert, ScrollView} from 'react-native';

class RegisterUserPage extends Component{

  constructor(){
    super();

    this.state = {
        name: '',
        lastname: '',
        email: '',
        password: '',
        passwordConfirm: '',
        organism: '',
        placeProcedence: '',
    }
  }

  handleRegisterUser(){

    alert('registrando usuario');

  }

  render(){

    const {textTitleStyle, inputUsuario} = styles;

    return(
      <View>
        <Text style={textTitleStyle}>Nuevo usuario</Text>
        <TextInput
          style={inputUsuario}
          placeholder="Apellido :"
          onChangeText={(lastname) => this.setState({lastname})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="Nombre :"
          onChangeText={(name) => this.setState({name})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="E-mail :"
          onChangeText={(email) => this.setState({email})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="Constraseña :"
          onChangeText={(password) => this.setState({password})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="Confirmar Contraseña :"
          onChangeText={(passwordConfirm) => this.setState({passwordConfirm})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="Organismo :"
          onChangeText={(organism) => this.setState({organism})}
        />
        <TextInput
          style={inputUsuario}
          placeholder="Lugar procedencia :"
          onChangeText={(placeProcedence) => this.setState({placeProcedence})}
        />

        <Button
          title="ACEPTAR"
          color="green"
          onPress={() => this.handleRegisterUser()}
        />

      </View>
    )
  }

}
 const styles = {
   textTitleStyle: {
     textAlign: 'center',
     fontSize: 20,
     fontFamily: 'serif',
     marginTop: 20,
   },
   inputUsuario: {
     height: 50,
     marginTop: 5,
     padding:15,
   },
 }


export default RegisterUserPage;
